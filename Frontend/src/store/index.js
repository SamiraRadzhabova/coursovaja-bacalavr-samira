import { createStore } from "vuex";
export default createStore({
  strict: process.env.NODE_ENV !== "production",
  namespaced: true,
  state: {
    subadmins: [],
    permissions: [],
    catalogs: [],
    userCatalog: [],
    userPermission: [],
  },
  mutations: {
    // Отримання всіх субадмінів
    getSubadmins(state, subadmins) {
      state.subadmins = subadmins;
    },

    // Усі дозволи які є (для мультиселекту)
    getPermission(state, permissions) {
      state.permissions = permissions;
    },

    // Дані за кнопкою "Дозвіли" для певного користувача
    getUserPermission(state, userPermission) {
      state.userPermission = userPermission;
    },

    // Усі дозволи які є (для мультиселекту)
    getCatalogs(state, catalogs) {
      state.catalogs = catalogs;
    },

    // Дані за кнопкою "Дозвіли" для певного користувача
    getUserCatalog(state, userCatalog) {
      state.userCatalog = userCatalog;
    },
  },
});
