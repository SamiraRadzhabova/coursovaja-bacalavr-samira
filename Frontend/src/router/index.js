import { createRouter, createWebHistory } from "vue-router";
import Auth from "../views/Auth.vue";
import AdminPage from "../views/AdminPage.vue";
import Error from "../views/Error.vue";

const routes = [
  {
    path: "/auth",
    name: "auth",
    component: Auth,
  },
  {
    path: "/",
    name: "home",
    component: AdminPage,
  },
  {
    name: "404",
    path: "/:pathMatch(.*)*",
    component: Error,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("access_token");

  if (to.name !== "auth" && !token) {
    next({ name: "auth" });
  } else if (to.name === "home" && !token) {
    next({ name: "auth" });
  } else {
    next();
  }
});

export default router;
