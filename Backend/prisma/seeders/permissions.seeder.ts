import { Prisma, PrismaClient } from '@prisma/client';

const permissions: Prisma.PermissionCreateManyInput[] = [
  { name: 'Read', key: 'R' },
  { name: 'Write', key: 'W' },
  { name: 'Implementation', key: 'E' },
  { name: 'Addition', key: 'A' },
];

export const permissionsSeeder = async (prisma: PrismaClient) => {
  try {
    await prisma.permission.createMany({ data: permissions });
    console.log(`${permissions.length} permissions have been seeded`);
  } catch {
    console.log(`Permissions have not been seeded`);
  }
};
