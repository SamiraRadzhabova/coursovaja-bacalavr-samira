import { Prisma, PrismaClient } from '@prisma/client';

const catalogs: Prisma.CatalogsCreateManyInput[] = [
  { name: 'A', text: '' },
  { name: 'B', text: '' },
  { name: 'C', text: '' },
  { name: 'D', text: '' },
  { name: 'E', text: '' },
];

export const catalogsSeeder = async (prisma: PrismaClient) => {
  try {
    await prisma.catalogs.createMany({ data: catalogs });
    console.log(`${catalogs.length} catalogs have been seeded`);
  } catch {
    console.log(`Catalogs have not been seeded`);
  }
};
