import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsDefined,
  IsEmail,
  IsString,
  Length,
} from 'class-validator';
import { EntityAdminExists } from '../../helpers/validation/entity-admin-exists';

export class CreateSubAdminDto {
  @Transform(({ value }) =>
    typeof value === 'string' ? value.toLowerCase() : value,
  )
  @IsDefined({ message: "Пошта є обов'язковим полем для вводу" })
  @IsEmail({}, { message: 'Будь ласка, введіть правильний формат пошти' })
  @Length(7, 100, {
    message: 'Пошта має бути довжиною від 7 до 70 символів',
  })
  @EntityAdminExists('user', 'email', false)
  @ApiProperty({ example: 'adam@gmail.com' })
  email: string;
  @IsDefined({ message: "Ім'я є обов'язковим полем для вводу" })
  @IsString({ message: "Ім'я має бути строкою" })
  @Length(2, 50, {
    message: "Ім'я має бути довжиною від 2 до 50 символів",
  })
  @ApiProperty({ example: 'Adam' })
  first_name: string;
  @IsDefined({ message: "Прізвище є обов'язковим полем для вводу" })
  @IsString({ message: 'Прізвище має бути строкою' })
  @Length(2, 50, {
    message: 'Прізвище має бути довжиною від 2 до 50 символів',
  })
  @ApiProperty({ example: 'Smith' })
  last_name: string;
  @IsDefined({ message: "Пароль є обов'язковим полем для вводу" })
  @Length(8, 32, {
    message: 'Пароль має бути довжиною від 8 до 32 символів',
  })
  @IsString({ message: 'Пароль має бути строкою' })
  @ApiProperty({ example: 'MySr0ngP@ssw0rd' })
  password: string;
  @IsDefined()
  @IsArray()
  @ArrayMinSize(1, { message: 'Ви повинні обрати принаймі одну пермісію' })
  @EntityAdminExists('permission')
  @ApiProperty({ example: [1, 2] })
  permission_ids: number[];
  @IsDefined()
  @IsArray()
  @ArrayMinSize(1, { message: 'Ви повинні обрати принаймі один каталог' })
  @EntityAdminExists('catalogs')
  @ApiProperty({ example: [1, 2] })
  catalog_ids: number[];
}
