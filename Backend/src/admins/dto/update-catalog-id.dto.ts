import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';

export class UpdateCatalogIDDto {
  @IsDefined()
  @Type(() => Number)
  @IsNumber()
  @ApiProperty({ example: 1 })
  catalog_id: number;
}
