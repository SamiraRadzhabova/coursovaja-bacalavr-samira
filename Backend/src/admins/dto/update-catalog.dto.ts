import { ApiProperty } from '@nestjs/swagger';
import { IsDefined, IsString } from 'class-validator';

export class UpdateCatalogDto {
  @IsDefined()
  @IsString()
  @ApiProperty({ example: 1 })
  text: string;
}
