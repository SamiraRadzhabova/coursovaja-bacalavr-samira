import { Injectable } from '@nestjs/common';
import { PrismaAdminService } from 'src/prisma/prisma-admin.service';

import { CreateSubAdminDto } from './dto/create-sub-admin.dto';
import { UpdateSubAdminDto } from './dto/update-sub-admin.dto';
import { Prisma } from '@prisma/client';
import { FileHelper } from 'src/helpers/file-system/file-helper';
import { SnakeCaseToLocalText } from 'src/helpers/const-to-text';
import { AuthService } from 'src/auth/auth.service';

import * as CryptoJS from 'crypto-js';

@Injectable()
export class AdminsService {
  constructor(
    private readonly prismaAdmin: PrismaAdminService,
    private readonly fileHelper: FileHelper,
    private readonly authService: AuthService,
  ) {}

  async getPermissions() {
    const permissions = await this.prismaAdmin.permission.findMany({
      select: { id: true, key: true, name: true },
    });
    return permissions.map(({ id, key, name }) => ({
      id,
      name: SnakeCaseToLocalText(name),
      key,
    }));
  }

  async getCatalogs() {
    const catalogs = await this.prismaAdmin.catalogs.findMany({
      select: { id: true, name: true, text: true },
    });
    return catalogs;
  }

  async createSubAdmin({
    permission_ids,
    catalog_ids,
    password,
    ...dto
  }: CreateSubAdminDto) {
    const data: Prisma.UserCreateInput = {
      ...dto,
      password: this.authService.hashPassword(password),
      user_permissions: {
        createMany: {
          data: permission_ids.map((permission_id) => ({
            permission_id,
          })),
        },
      },
      user_catalogs: {
        createMany: {
          data: catalog_ids.map((catalog_id) => ({
            catalog_id,
          })),
        },
      },
    };
    await this.prismaAdmin.user.create({
      data,
      select: { email: true },
    });
    return { status: 'success' };
  }

  async updateSubAdmin(
    subAdminId: number,
    { permission_ids, catalog_ids, ...dto }: UpdateSubAdminDto,
  ) {
    const data: Prisma.UserUpdateInput = {
      ...dto,
      user_permissions: permission_ids
        ? {
            createMany: {
              data: permission_ids.map((permission_id) => ({
                permission_id,
              })),
            },
          }
        : undefined,
      user_catalogs: catalog_ids
        ? {
            createMany: {
              data: catalog_ids.map((catalog_id) => ({
                catalog_id,
              })),
            },
          }
        : undefined,
    };
    await this.prismaAdmin.$transaction([
      this.prismaAdmin.userPermission.deleteMany({
        where: { user_id: subAdminId },
      }),
      this.prismaAdmin.userCatalogs.deleteMany({
        where: { user_id: subAdminId },
      }),
      this.prismaAdmin.user.update({
        where: { id: subAdminId },
        data,
        select: { id: true },
      }),
    ]);
    return { status: 'success' };
  }

  async getSubAdmins() {
    const subAdmins = await this.prismaAdmin.user.findMany({
      where: { role: 'SUB_ADMIN' },
      select: {
        id: true,
        first_name: true,
        last_name: true,
        email: true,
        createdAt: true,
        updatedAt: true,
      },
    });
    return subAdmins;
  }

  async getSubAdmin(subAdminId: number) {
    const { user_permissions, user_catalogs, ...subAdmin } =
      await this.prismaAdmin.user.findUnique({
        where: { id: subAdminId },
        select: {
          id: true,
          first_name: true,
          last_name: true,
          email: true,
          user_permissions: {
            select: { permission: { select: { id: true, name: true } } },
          },
          user_catalogs: {
            select: { catalogs: { select: { id: true, name: true } } },
          },
        },
      });
    const result = {
      ...subAdmin,
      permissions: user_permissions.map(
        (user_permission) => user_permission.permission,
      ),
      catalogs: user_catalogs.map((user_catalog) => user_catalog.catalogs),
    };
    return result;
  }

  async deleteSubAdmin(subAdminId: number) {
    await this.prismaAdmin.user.delete({ where: { id: subAdminId } });
    return { status: 'success' };
  }

  async updateCatalog(catalog_id: number, text: string) {
    // Секретний ключ
    const secretKey = 'samira';
    text = CryptoJS.DES.encrypt(text, secretKey).toString();

    await this.prismaAdmin.catalogs.update({
      where: { id: catalog_id },
      data: { text },
    });
    return { status: 'success' };
  }
}
