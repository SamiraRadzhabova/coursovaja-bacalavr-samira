import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { IsDefined, IsEmail, IsNumber, IsString } from 'class-validator';

export class LoginDto {
  @Transform(({ value }) =>
    typeof value === 'string' ? value.toLowerCase() : value,
  )
  @IsDefined({ message: "Пошта є обов'язковим полем для вводу" })
  @IsEmail({}, { message: 'Невірна пошта' })
  @ApiProperty({ example: 'adam@gmail.com' })
  email: string;
  @IsDefined({ message: "Пароль є обов'язковим полем для вводу" })
  @IsString({ message: 'Пароль має бути строкою' })
  @ApiProperty({ example: 'MySr0ngP@ssw0rd' })
  password: string;
  @IsDefined({ message: "Пароль є обов'язковим полем для вводу" })
  @IsNumber({}, { message: '' })
  x: number;
  @IsDefined({ message: "Пароль є обов'язковим полем для вводу" })
  @IsNumber({}, { message: '' })
  y: number;
}
